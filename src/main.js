import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import helpers from './helpers';
import './registerServiceWorker'

Vue.config.productionTip = false

new Vue({
  mixins: [helpers],
  router,
  store,
  render: h => h(App)
}).$mount('#app')
