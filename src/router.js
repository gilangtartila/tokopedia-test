import Vue from 'vue'
import Router from 'vue-router'
import ListPokemon from './views/ListPokemon.vue'
import MyPokemon from './views/MyPokemon.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'ListPokemon',
      component: ListPokemon
    },
    {
      path: '/my-pokemon',
      name: 'MyPokemon',
      component: MyPokemon
    }
  ]
})
