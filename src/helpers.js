import Vue from 'vue'
import { mapState } from "vuex"
import axios from 'axios'

const helpers = Vue.mixin({
    computed: {
        ...mapState([
            'serviceAPI',
            'open_detail',
            'take',
            'update',
            'my_pokemon'
        ])
    },
    methods: {
        convertName(name) {
          try {
            return name.toLowerCase().replace(/(^|\s)\S/g, first_word => first_word.toUpperCase())
          } catch {
            return name
          }
        },
        getCountPokemon(array) {
          let my_pokemon = JSON.parse(localStorage.getItem('my_pokemon'))
    
          var filtered = my_pokemon.filter((val) => {
              return val.data.name === array.name;
          });
          
          return filtered.length
        },
        async getPokemonDetail(name) {
          return await axios.get(this.serviceAPI + '/pokemon/' + name)
        },
        removePokemon(key, position) {
          var get_index = this.my_pokemon.findIndex(x => x.nickname == key)
          this.my_pokemon.splice(get_index, 1) 
          localStorage.setItem('my_pokemon', JSON.stringify(this.my_pokemon))
          if (position == 2) {
            this.$router.go()
          }
        },
        getObject(o) {
            var result = Object.assign({}, o)
            return result
        },
        takePokemon(data) {
            this.$store.commit('handleTake', true)
            this.$store.commit('handleDetail', data)
        },
        openDetail(data) {
            this.$store.commit('handleDetail', data)
        },
        closeAll() {
            this.$store.commit('handleDetail', null)
            this.$store.commit('handleTake', false)
        },
        updateNickname() {
            this.$store.commit('handleUpdate', true)
        },
        closeUpdate() {
            this.$store.commit('handleUpdate', false)
        }
    },
    mounted() {
      if (!localStorage.getItem('my_pokemon')) {
        localStorage.setItem('my_pokemon', JSON.stringify([]))
      }
    },
});

export default helpers;
