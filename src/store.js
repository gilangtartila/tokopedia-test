import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    serviceAPI: 'https://pokeapi.co/api/v2',
    open_detail: null,
    take: false,
    update: false,
    my_pokemon: JSON.parse(localStorage.getItem('my_pokemon'))
  },
  mutations: {
    handleTake(state, changed) {
      state.take = changed
    },
    handleUpdate(state, changed) {
      state.update = changed
    },
    handleDetail(state, changed) {
      state.open_detail = changed
    }
  }
})
